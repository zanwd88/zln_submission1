<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ZLN | Form</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery.fancytable/dist/fancyTable.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css">
</head>
<body>
    <?php
        $host = "zlntest.database.windows.net";
        $user = "zlnadmin";
        $pass = "DD1234zz";
        $db = "zlntest";

        try {
            $conn = new PDO("sqlsrv:server = $host; Database = $db", $user, $pass);
            $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            echo "Database Status: Connected";
        } catch(Exception $e) {
            echo "Database Status: Disconnected";
            echo "Error: " . $e;
        }

        if (isset($_POST["submit_user"])) {
            $firstname = $_POST['inputFirstname'];
            $lastname = $_POST['inputLastname'];
            $email = $_POST["inputemail"];
            $queryInsert = "INSERT INTO [dbo].[user] (firstname, lastname, email) 
                            VALUES ('$firstname','$lastname','$email')";
            $insert = $conn->prepare($queryInsert);
            $insert->execute();

            if($insert){
                ?>
                    <script>alert('Data Entry Success')</script>
                <?php
            }else{
                ?>
                    <script>alert('Data Entry Failed')</script>
                <?php
            }
            
        }
    ?>

    <div class="container" style="margin-top: 50px; width: 30%;">
        <form method="POST" action="" style="border: 1px solid #e2e2e2; border-radius: 10px; padding: 20px;">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm">
                        <label for="inputFirstname">First Name</label>
                        <input type="text" class="form-control" name="inputFirstname" id="inputFirstname" placeholder="Enter First Name" required>
                    </div>
                    <div class="col-sm">
                        <label for="inputLastname">Last Name</label>
                        <input type="text" class="form-control" name="inputLastname" id="inputLastname" placeholder="Enter Last Name" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputemail">Email address</label>
                <input type="email" class="form-control" name="inputemail" id="inputemail" placeholder="Enter email" required>
            </div>
            <button type="submit" class="btn btn-secondary">Reset</button>
            <button type="submit" name="submit_user"class="btn btn-primary">Submit</button>
        </form>
        <?php   
            
        ?>
    </div>
    <div class="container" style="margin-top: 50px; width: 60%;">
        <h3 align="center">USER DATA</h3>
        <table class="table" id="user_data" style="margin-top: 30px;">
            <thead >
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Date Entry</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $querySelect = "SELECT * FROM [dbo].[user]";
                    $select = $conn->query($querySelect);
                    if( $select === false) {
                        die( print_r( sqlsrv_errors(), true) );
                    }
                    
                    while( $row = $select->fetch(PDO::FETCH_ASSOC) ) {
                        ?>
                        <tr>
                            <th scope="row"><?=$row['id']?></th>
                            <td><?=$row['firstname']?></td>
                            <td><?=$row['lastname']?></td>
                            <td><?=$row['email']?></td>
                            <td><?=$row['date_entry']?></td>
                        </tr>
                        <?php
                    }
                ?>
                
            </tbody>
        </table>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#user_data").fancyTable({
                    sortColumn:0,
                    pagination: true,
                    perPage:5,
                    searchable: false,
                });		
            });
        </script>
    </div>
</body>
</html>

